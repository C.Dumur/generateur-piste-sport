from pydub import AudioSegment
import os
import random

def get_random_track(dir, path):
    track_name = random.choice(dir)
    audio = AudioSegment.from_mp3(path + "/" + track_name)
    return {'name': track_name, 'audio': audio}

top = AudioSegment.from_mp3("bruitage/bip.mp3")
path = 'music'
liste_fichier = os.listdir(path)

# entrée des utilisateurs
playlist_duration = 0
while playlist_duration == 0:
    playlist_duration = int(input('Entrez la durée de piste (en minute)'))
    if playlist_duration == 0 :
        print("valeur incorrecte")
 

top_interval = 0
while top_interval == 0:
    top_interval = int(input('Entrez le temps entre les tops (en seconde)'))
    if top_interval == 0 :
        print("valeur incorrecte")

#Création de la piste
print('Génération de la piste vide')
playlist = AudioSegment.empty()

#Ajout des chansons
first_track = get_random_track(liste_fichier, path)
print("ajout de la piste " + first_track['name'])
playlist += first_track['audio']

# On génère une piste en concatennant des morceaux
temps_piste = playlist_duration * 60

while playlist.duration_seconds < temps_piste:
    next_track = get_random_track(liste_fichier, path)
    print('Ajout de la piste ' + next_track['name'])
    playlist = playlist.append(next_track['audio'], crossfade=5000)
    print('durée de la piste : ' + str(playlist.duration_seconds) + 's')

print("fin de la génération")

interval_top = top_interval * 1000
last_top = 0
print('Ajout des bip')
while last_top + interval_top < len(playlist):
    last_top += interval_top
    playlist = playlist.overlay(top, last_top)
    print("ajout d'un top à " + str(last_top) + 'ms')

print("fin de l'ajout des tops")

print("Ecriture du fichier")
file_handle = playlist.export("final.mp3", format="mp3")