# Générateur de playlist minutées en python

## Instalation :

Création et activation du venv
```
virtualenv -p python3 venv
cd venv
source bin/activate
```

Instalation de pydub
```
cd ..
pip install -r requirements.txt
```

Attention : les paquets ffmpeg et libavcodec-extra sont nécessaires à pydub, pour les installer

```
sudo apt-get install ffmpeg libavcodec-extra
```

## Génération de piste

Ajouter les pistes de musique, en MP3, dans le dossier music
Ajouter le bruitage du top, en MP3, dans le dossier bruitage sous le nom bip.mp3

Pour générer une piste :
```
python3 main.py
```

Puis entrer le temps minimale de la piste et le temps d'espace entre les tops
À la fin de l'exécution, un fichier final.mp3 est généré à la racine du projet